#!/bin/bash
DOCKER_IMAGE=paulabeatrizolmedo/remote-mole

sudo docker run --rm -it \
	-v $PWD/remote-mole/.config:.config/remote-mole \
	-w $PWD \
	$DOCKER_IMAGE bash